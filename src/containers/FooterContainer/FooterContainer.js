import React from "react";
import FooterLogo from "../../images/footerLogo.svg";
import FbLogo from "../../images/facebook.svg";
import LinkedinLogo from "../../images/linkedin.svg";
import TwitterLogo from "../../images/twitter.svg";
import './FooterContainer.css';

const FooterContainer = () => {
  return (
    <div className="footer-wrapper">
      <div className="footer-contacts">
        <p>
          <a className="linkable" href="#">
            კონტაქტი
          </a>
        </p>
        <p>
          <a className="linkable" href="#">
            კონტაქტი
          </a>
        </p>
      </div>
      <div className="logo-wrapper">
        <img src={FooterLogo} alt="logo" />
      </div>
      <div className="socialss">
        <img className="social-logos--fb" src={FbLogo} alt="fblogo" />
        <img className="social-logos" src={LinkedinLogo} alt="lklogo" />
        <img className="social-logos" src={TwitterLogo} alt="twitLogo" />
      </div>
    </div>
  );
};

export default FooterContainer;
