import React from "react";
import Logo from "../../images/logo.svg";
import LanguageIcon from "../../images/language.svg";
import "./HeaderContainer.css";
import Box from "@mui/material/Box";
import Popper from "@mui/material/Popper";
import { Api } from "../../services/Services";
const HeaderContainer = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchEl, setAnchEl] = React.useState(null);
  const [anchElThird, setAnchElThird] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleClickOver = () => {
    setAnchorEl(null);
  };

  const handleClickAnch = (event) => {
    setAnchEl(anchEl ? null : event.currentTarget);
  };

  const handleClickOverAnch = () => {
    setAnchEl(null);
  };

  const handleClickAnchThird = (event) => {
    setAnchElThird(anchElThird ? null : event.currentTarget);
  };

  const handleClickOverAnchThird = () => {
    setAnchElThird(null);
  };

  const openAnchThird = Boolean(anchElThird);
  const idAnchThird = openAnchThird ? "simple-popper--diff" : undefined;

  const openAnch = Boolean(anchEl);
  const idAnch = openAnch ? "simple-popper--diff" : undefined;

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <header>
      <div className="logo">
        <img src={Logo} alt="logo" />
      </div>
      <div className="navigation-wrapper">
        <img className="language-logo" src={LanguageIcon} alt="lang-icon" />
        <nav>
          <ul className="nav-ul">
            <li className="ul-li">
              <a className="linkable" href="/">
                მთავარი
              </a>
            </li>
            <li className="ul-li">
              <a className="linkable" href="#">
                პარტია
              </a>
            </li>
            <li className="ul-li">
              <button
                aria-describedby={id}
                type="button"
                onMouseOver={handleClick}
                className="linkable"
              >
                ორგანიზაციები
              </button>
              <Popper id={id} open={open} anchorEl={anchorEl}>
                <Box
                  sx={{
                    border: "none",
                    p: 1,
                    bgcolor: "#EBEBEB",
                    borderRadius: "0 0 20px 20px",
                  }}
                >
                  <div
                    onMouseLeave={handleClickOver}
                    className="organization-poper"
                  >
                    <a className="organization-poper--first" href="#">
                      თბილისის ორგანიზაცია
                    </a>
                    <a className="organization-poper--children" href="#">
                      რეგიონალური ორგანიზაცია
                    </a>
                    <a className="organization-poper--children" href="#">
                      ქალთა ორგანიზაცია
                    </a>
                    <a className="organization-poper--last" href="#">
                      ახალგაზრდული
                    </a>
                  </div>
                </Box>
              </Popper>
            </li>
            <li className="ul-li">
              <button
                aria-describedby={idAnch}
                type="button"
                onMouseOver={handleClickAnch}
                className="linkable"
              >
                წარმომადგენლობა
              </button>
              <Popper id={idAnch} open={openAnch} anchorEl={anchEl}>
                <Box
                  sx={{
                    border: "none",
                    p: 1,
                    bgcolor: "#EBEBEB",
                    borderRadius: "0 0 20px 20px",
                  }}
                >
                  <div
                    onMouseLeave={handleClickOverAnch}
                    className="organization-popper--diff"
                  >
                    <a className="organization-poper--first" href="#">
                      პარლამენტი
                    </a>
                    <a className="organization-poper--last" href="#">
                      საკრებულო
                    </a>
                  </div>
                </Box>
              </Popper>
            </li>
            <li className="ul-li">
              <button
                aria-describedby={idAnchThird}
                type="button"
                onMouseOver={handleClickAnchThird}
                className="linkable"
              >
                მედია
              </button>
              <Popper
                id={idAnchThird}
                open={openAnchThird}
                anchorEl={anchElThird}
              >
                <Box
                  sx={{
                    border: "none",
                    p: 1,
                    bgcolor: "#EBEBEB",
                    borderRadius: "0 0 20px 20px",
                  }}
                >
                  <div
                    onMouseLeave={handleClickOverAnchThird}
                    className="organization-popper--diff"
                  >
                    <a className="organization-poper--first" href="#">
                      სიახლეები
                    </a>
                    <a className="organization-poper--last" href="#">
                      დოკუმენტაცია
                    </a>
                  </div>
                </Box>
              </Popper>
            </li>
          </ul>
        </nav>
        <li className="donation">
          <a href="#">დონაცია</a>
        </li>
        <li className="joinUs">
          <a href="#">შემოგვიერთდი</a>
        </li>
      </div>
    </header>
  );
};

export default HeaderContainer;
