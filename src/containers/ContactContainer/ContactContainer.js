import React from "react";
import Circle from "../../images/yellowCircle.svg";
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import "./ContactContainer.css";

const Contact = () => {
  return (
    <div className="contact-wrapper">
      <div className="contact-left-section-wrapper">
        <div className="heading-wrapper">
          <img className="heading-img" src={Circle} alt="circle" />
          <h2 className="heading-title">კონტაქტი</h2>
        </div>
        <div className="contact-info-wrapper">
          <p className="contact-info">
            სამუშაო განრიგი: ორშაბათი - პარასკევი, 10:00 - 19:00
          </p>
          <p className="contact-info">თბილისი, ცენტრალი</p>
          <p className="contact-info">
            ტელეფონი{" "}
            <span className="contact-info--colored">032 223 22 88</span>
          </p>
          <p className="contact-info">
            ელ.ფოსტა{" "}
            <span className="contact-info--colored">info@lelo2020.com</span>
          </p>
        </div>
        <div className="inputs-wrapper">
          <input className="text-inputs" type="text" placeholder="სახელი" />
          <input className="text-inputs" type="text" placeholder="გვარი" />
          <input className="email-input" type="email" placeholder="ელ.ფოსტა" />
        </div>
        <input type="text" className="textarea-input" placeholder="ტექსტი" />
        <ButtonContainer name="გაგზავნა" />
      </div>
      <iframe
        className="map"
        src="https://www.google.com/maps/d/embed?mid=134qjUWDIAW2aJWVO2iahYzCln60&ehbc=2E312F"
        width="960"
        height="560"
      ></iframe>
    </div>
  );
};

export default Contact;
