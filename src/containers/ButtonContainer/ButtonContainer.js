import React from "react";
import './ButtonContainer.css';

const ButtonContainer = ({ name, buttonStyle }) => {
    return (
        <div className={buttonStyle ? 'sessionBtn' : 'topContentBtn'}>
            {name}
        </div>
    )
};

export default ButtonContainer;