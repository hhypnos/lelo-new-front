import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import Building from "../../images/bank.svg";
import Circle from "../../images/yellowCircle.svg";
import FooterContainer from "../FooterContainer/FooterContainer";
import "./AboutUsContainer.css";

const AboutUsContainer = (props) => {
  return (
    <div className="main--wrapper">
      <HeaderContainer />
      <div className="wrapper-styles">
        <div className="header-image--wrapper">
          <img src={Building} alt="building" className="aboutUsImg" />
          <div style={{ marginTop: "200px" }}>
            <div className="title-wrapperFirst">
              <img className="title-img" src={Circle} alt="circle" />
              <h2 className="title-heading">ჩვენს შესახებ</h2>
            </div>

            <div style={{ width: "80%", padding: "10px" }}>
              <p className="fontStyles">{props.readMoreData[0].description}</p>
              <p className="fontStyles">{props.readMoreData[0].description}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="grey-content-wrapper">
        <div className="content_wrapper">
          <div className="title-wrapper">
            <img className="title-img" src={Circle} alt="circle" />
            <h2 className="title-heading">ჩვენს შესახებ</h2>
          </div>
          <div>
            <p className="fontStyles">{props.readMoreData[0].description}</p>
            {/* <p className="fontStyles">{props.readMoreData[0].description}</p> */}
          </div>
        </div>
      </div>
      <FooterContainer />
    </div>
  );
};

export default AboutUsContainer;
