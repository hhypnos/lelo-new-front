import React from "react";
import Date from "../../../images/date.svg";
import "./NewsCard.css";
import { useNavigate } from "react-router-dom";

const NewsCard = ({ image, title, description, date, id }) => {
  const navigate = useNavigate();

  const redirect = React.useCallback(() => {
    navigate(`/read-more/${id}`);
  }, [navigate, id]);

  return (
    <div
      className="single-card"
      style={{
        width: 378,
        height: 610,
        position: "relative",
        borderRadius: 25,
      }}
    >
      <div className="single-card-decoration"></div>
      <img className="card-img " src={image} alt="cardimg" width="280px" height="307px" />
      <div className="card-date-wrapper">
        <img src={Date} alt="date" />
        <p>{date}</p>
      </div>
      <div className="card-description-wrapper">
        <h3 className="card-description-title ">{title}</h3>
        <p className="card-description">{description}</p>
        <p onClick={redirect} className="read-more">
          Read More
        </p>
      </div>
    </div>
  );
};

export default NewsCard;
