import React from "react";
import Circle from "../../images/yellowCircle.svg";
import NewsCard from "./NewsCard/NewsCard";
import './NewsContainer.css';
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import { useNavigate } from "react-router-dom";
import TeamCard from "../TeamContainer/TeamCard/TeamCard";

const NewsContainer = (props) => {
    console.log(props,"prrr");
  const newsCardsList = props?.newsData.map(event => (
    <NewsCard
      key={event.id}
      id={event.id}
      image={event?.cover?.upload.full_path}
      title={event.title}
      description={event.description}
      date={event.date}
    />
  ));
  const navigate = useNavigate();
  const redirect = () => {
    navigate('/more')
  }

  return (
      <>
          {props.loadingNews ? (
                  <di>sodsd</di>
              ) :(
    <div style={{paddingTop: 85, paddingBottom: 65}}>
      <div className="heading-wrapper">
        <img className="heading-img" src={Circle} alt="circle" />
        <h2 className="heading-title">სიახლეები</h2>
      </div>
      <div
        className="card-list-wrapper"
      >
        <div>
         <div className="card-list-styles">{newsCardsList}</div>
        </div>
      </div>
      <div onClick={redirect}>
        <ButtonContainer
          name='მეტის ნახვა'
          buttonStyle={true}
        />
      </div>
    </div>
          )}
      </>
  );
};

export default NewsContainer;
