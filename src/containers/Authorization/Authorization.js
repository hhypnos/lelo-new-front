import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import Circle from "../../images/yellowCircle.svg";
import FooterContainer from "../FooterContainer/FooterContainer";
import './Authorization.css';

const Authorization = () => {
  return (
    <div className="auth-wrapper">
      <div className="header__wrapper">
        <HeaderContainer />
      </div>
      <div
        className="election--wrapper"
      >
        <div className="auth-title-wrapper">
          <img className="title-img" src={Circle} alt="circle" />
          <h2 className="title-heading">შიდა არჩევნები</h2>
        </div>
      </div>
      <p className="election--description">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry’s standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum.
      </p>
      <h2
        className="log-in-styles"
      >
        გაიარეთ ავტორიზაცია
      </h2>
      <div className="entire-form">
        <div className="form--wrapper">
          <input
            className="input-email"
            type="email"
            placeholder="ელ-ფოსტა"
          />
          <div
            className="send-code-wrapper"
          >
            <input
              className="input-phone"
              type="number"
              placeholder="ტელეფონი"
            />
            <button
              className="send-code"
            >
              კოდის გაგზავნა
            </button>
          </div>
          <div
            className="insert-code-wrapper"
          >
            <label className="label-styles">
              * გთხოვთ შეიყვანოთ მობილურ ნომერზე გამოგზავნილი ვერიფიკაციის კოდი
            </label>
            <input
              className="code-btn"
              type="number"
              placeholder="კოდი"
            />
          </div>
          <div
            className="log-in-wrapper"
          >
            <button
              className="log-in-btn"
            >
              შესვლა
            </button>
          </div>
        </div>
      </div>
      <FooterContainer />
    </div>
  );
};

export default Authorization;
