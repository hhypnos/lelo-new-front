import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import Circle from "../../images/yellowCircle.svg";
import "./ElectionsFilter.css";
import FooterContainer from "../FooterContainer/FooterContainer";

const ElectionsFilter = () => {
  return (
    <div className="auth-wrapper">
      <div className="header__wrapper">
        <HeaderContainer />
      </div>
      <div className="election--wrapper">
        <div className="auth-title-wrapper">
          <img className="title-img" src={Circle} alt="circle" />
          <h2 className="title-heading">შიდა არჩევნები</h2>
        </div>
      </div>
      <p className="election--description">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry’s standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum.
      </p>
      <div className="election-btns">
        <div className="election-signle-btn">არჩევნები 1</div>
        <div className="election-signle-btn">არჩევნები 2</div>
        <div className="election-signle-btn">არჩევნები 3</div>
        <div className="election-signle-btn">არჩევნები 4</div>
      </div>
      <FooterContainer />
    </div>
  );
};

export default ElectionsFilter;
