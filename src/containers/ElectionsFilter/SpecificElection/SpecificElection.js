import React from "react";
import HeaderContainer from "../../HeaderContainer/HeaderContainer";
import Circle from "../../../images/yellowCircle.svg";
import "./SpecificElection.css";
import FooterContainer from "../../FooterContainer/FooterContainer";

const SpecificElection = (props) => {
  const teamCardList = props.teamsData.map((event) => {
    return (
      <div key={event.id} className="specific-election-wrapper">
        <div style={{}}>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              boxShadow: "0px 5px 20px rgba(0,0,0, 0.1)",
              borderRadius: 54,
              padding: 20,
              gap: 25,
            }}
          >
            <img className="election-image" src={event.image} alt="img" />
            <div className="election-details">
              <h6 className="election-details--heading">შიდა არჩევნები</h6>
              <p className="election-details--text">{event.description}</p>
              <button className="election-details--btn">ხმის მიცემა</button>
            </div>
          </div>
        </div>
      </div>
    );
  });

  return (
    <div className="auth-wrapper">
      <div className="header__wrapper">
        <HeaderContainer menu={props.menu} />
      </div>
      <div className="election--wrapper">
        <div className="auth-title-wrapper">
          <img className="title-img" src={Circle} alt="circle" />
          <h2 className="title-heading">შიდა არჩევნები</h2>
        </div>
      </div>
      <p className="election--description">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry’s standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum.
      </p>
      <div className="election-btns">
        <div className="election-signle-btn--yellow">არჩევნები 1</div>
      </div>
      <div
        style={{
          width: "62%",
          margin: "0 auto",
          display: "flex",
          flexWrap: "wrap",
          gap: 25,
        }}
      >
        {teamCardList}
      </div>
      <div className="specific-election-footer">
        <FooterContainer />
      </div>
    </div>
  );
};

export default SpecificElection;
