import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import DateIcon from "../../images/date.svg";
import "./ReadMoreContainer.css";
import FooterContainer from "../FooterContainer/FooterContainer";
import { useParams } from "react-router-dom";
import {Api} from "../../services/Services";
const ReadMoreContainer = (props) => {
    const [allNews, setAllNews] = React.useState([]);
    const [news, setNews] = React.useState({});
  const { id } = useParams();
  console.log(id, "asdasdasd");
  React.useEffect(() => {
        Api.getData(`blogs/${id}`).then((json) => {
            const res = json.result.data;

            setNews(res);
        });
        Api.getData(`blogs?order_by=asc&type_id=1&lang_id=1&data_per_page=4`).then(
            (json) => {
                const res = json.result.data;
                console.log(res, "ssss");
                setAllNews(res);
            }
        );
    }, [id]);
  // const data = props.newsData.find((data) => data.id == id);
  // console.log(data, "prop");

  // for navigate with :id u just need to use variable data
  const readMoreList =  (
    <div key={news.id} className="container-wrapper">
      <div className="topcontent-wrapper">
        <div className="heading--wrapper">
          <div className="wrap">
            <h2 className="heading-styles">{news.title}</h2>
            <div className="cardContainer">
              <div className="dateWrapper">
                <img src={DateIcon} alt="data" />
                <p className="dateSize">{news.date}</p>
              </div>
              <div className="card-decoration"></div>
            </div>
          </div>
        </div>
        <img src={news.cover?.upload.full_path} alt="img" className="newsDetailImg" />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <div className="descriptions">{news.description}</div>
        <div className="descriptions">{news.description}</div>
      </div>
    </div>
  );
  return (
    <div>
      <HeaderContainer menu={props.menu} />
      <div className="mappedData">{readMoreList}</div>
      <div className="footer">
        <FooterContainer />
      </div>
    </div>
  );
};

export default ReadMoreContainer;
