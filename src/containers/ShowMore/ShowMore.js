import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import NewsCard from "../NewsContainer/NewsCard/NewsCard";
import Circle from "../../images/yellowCircle.svg";
import Search from "../../images/search.svg";
import AbsoluteLine from "./AbsoluteLine";
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import FooterContainer from "../FooterContainer/FooterContainer";
import "./ShowMore.css";
import {Api} from "../../services/Services";

const ShowMore = (props) => {
    const [news, setNews] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        Api.getData(`blogs?order_by=asc&type_id=1&lang_id=1&data_per_page=4`).then(
            (json) => {
                const res = json.result.data;
                console.log(res, "res");
                setNews(res);
                console.log(news,'sss')
                setLoading(false);
            }
        );
    }, []);

  const showMoreList = news.map((event) => (
    <div>
      <div className="first-row ">
        <NewsCard
          key={event.id}
          id={event.id}
          image={event.cover?.upload.full_path}
          title={event.title}
          date={event.date}
        />
      </div>
      <div className="second-row">
        <NewsCard
          key={event.id}
          id={event.id}
          image={event.cover?.upload.full_path}
          title={event.title}
          date={event.date}
        />
      </div>
    </div>
  ));
  const descriptionData = news.map((event) => {
    return <div className="card-description">{event.description}</div>;
  });
  return (
    <div className="showMore-wrapper">
      <div className="content--wrapper">
        <HeaderContainer />
        <div className="news--wrapper">
          <div className="news-search-wrapper">
            <div className="news-logo">
              <img src={Circle} alt="circle" />
              <h2 className="news--title">სიახლეები</h2>
            </div>
            <div className="search-wrapper">
              <input
                className="input-styles"
                type="search"
                placeholder="ძებნა"
              />
              <img className="search-icon" src={Search} alt="search" />
            </div>
          </div>
        </div>
      </div>
      <div className="absoluteLines-wrapper">
        <AbsoluteLine />
        <AbsoluteLine />
        <AbsoluteLine />
        <AbsoluteLine />
      </div>
      <div className="card-list--wrapper">{showMoreList}</div>
      <ButtonContainer name="მეტის ნახვა" buttonStyle={true} />
      <div className="footer-div">
        <FooterContainer />
      </div>
    </div>
  );
};

export default ShowMore;
