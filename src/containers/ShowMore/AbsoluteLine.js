import React from "react";

const AbsoluteLine = () => {
  return (
    <div
      style={{
        width: 280,
        height: 42,
        borderRadius: 21,
        backgroundColor: "#EBEBEB",
      }}
    ></div>
  );
};

export default AbsoluteLine;
