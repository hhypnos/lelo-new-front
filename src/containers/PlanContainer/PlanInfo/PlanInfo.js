import React from "react";
import './PlanInfo.css';

const PlanInfo = ({ icon, title }) => {
  return (
    <div
      className="Planinfo"
    >
      <img className="img" src={icon} alt="icon" />
      <p className="Planinfo-text">{title}</p>
    </div>
  );
};

export default PlanInfo;
