import React from "react";
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import Circle from "../../images/yellowCircle.svg";
import "./ContentContainer.css";
import { useNavigate } from "react-router-dom";

const ContentContainer = ({ title, image, circle = null }) => {
  const navigate = useNavigate();

  const redirect = () => {
    navigate("/aboutUs");
  };

  const redirectOnElection = () => {
    navigate("/log-in");
  };

  return (
    <div className="container-wrapper">
      {circle == 1 ? (
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-cardAboutUs">
              <div className="title-wrapperAboutUs">
                <img className="title-img" src={Circle} alt="circle" />
                <h2 className="title-headingAboutus">{title}</h2>
              </div>
              <div className="description">
                Lorem Ipaaasum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been the industry’s
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five centuries, but
                alsocxcxcxxccxc the leap into electronic typesetting, remaining
                essentially unchanged. It was popularised in the 1960s with the
                release of
              </div>
              <div onClick={redirect}>
                <ButtonContainer name="ვრცლად" />
              </div>
            </div>
          </div>
          <div>
            <img src={image} alt="session" className="aboutUsImg" />
          </div>
        </div>
      ) : circle == 0 ? (
        <div className="second-wrapper">
          <div className="second-content-wrapper">
            <div className="second-content-card">
              <h2 className="title">{title}</h2>
              <div className="text">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry’s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of
              </div>
              <ButtonContainer name="ვრცლად" />
            </div>
          </div>
          <div className="contentContainer">
            <img src={image} alt="session" className="contentImg" />
          </div>
        </div>
      ) : (
        <div className="wrapper--black">
          <div className="content-wrapper">
            <div className="content-card">
              <div className="title-wrapper">
                <img className="title-img" src={Circle} alt="circle" />
                <h2 className="title-heading--black">{title}</h2>
              </div>
              <div className="description--black">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry’s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of
              </div>
              <div onClick={redirectOnElection}>
                <ButtonContainer name="ვრცლად" />
              </div>
            </div>
          </div>
          <div>
            <img src={image} alt="session" className="aboutUsImg" />
          </div>
        </div>
      )}
    </div>
  );
};

export default ContentContainer;
