import React from "react";
import HeaderContainer from "../HeaderContainer/HeaderContainer";
import Circle from "../../images/yellowCircle.svg";
import "./ShowMoreTeams.css";
import TeamCard from "../TeamContainer/TeamCard/TeamCard";
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import FooterContainer from "../FooterContainer/FooterContainer";
import {Api} from "../../services/Services";


const ShowMoreTeams = (props) => {
    const [teams, setTeams] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        Api.getData(`teams?order_by=asc&type_id=1&lang_id=1&data_per_page=4`).then(
            (json) => {
                const res = json.result.data;
                console.log(res, "res");
                setTeams(res);
                console.log(teams,'sss')
                setLoading(false);
            }
        );
    }, []);

  console.log(teams, "sdsdsd");
  const teamCardList = teams.map((event) => (
    <div className="first-teamCard">
      <TeamCard
        key={event.id}
        image={event.avatar.upload.full_path}
        title={event.full_name}
        description={event.position}
        id={event.id}
      />
    </div>
  ));

  const descriptionData = teams.map((event) => (
    <div className="card-description">{event.description}</div>
  ));

  return (
      <>
          {loading ? (
              <div>asdad</div>
          ) :(
    <div className="showMore-wrapper">
      <HeaderContainer />
      <div className="top-heading-wrapper">
        <div className="news-logo">
          <img src={Circle} alt="circle" />
          <h2 className="news--title">ჩვენი გუნდი</h2>
        </div>
      </div>
      <div className="description-data">{descriptionData}</div>
      <div className="card-list---wrapper">{teamCardList}</div>
      <ButtonContainer name="მეტის ნახვა" buttonStyle={true} />
      <div className="organizations">
        <div className="organization-wrapper">
          <p className="choose-organization">ქალთა ორგანიზაცია</p>
        </div>
        <div className="organization-wrapper">
          <p className="choose-organization">ახალგაზრდული ორგანიზაცია</p>
        </div>
      </div>
      <div className="footer-div">
        <FooterContainer />
      </div>
    </div>)}
          </>
  );
};

export default ShowMoreTeams;
