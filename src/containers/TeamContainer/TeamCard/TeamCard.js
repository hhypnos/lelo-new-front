import React from "react";
import { useNavigate } from "react-router-dom";

const TeamCard = ({ image, title, description, id }) => {
  const navigate = useNavigate();

  const redirect = React.useCallback(() => {
    navigate(`/specific-member/${id}`);
  }, [navigate, id]);

  return (
    <div className="single-card">
      <img
        style={{ marginBottom: 25 }}
        src={image}
        alt="teamMember"
        width="280px"
        height="307px"
      />
      <h3 style={{ marginBottom: 10, marginLeft: 25 }}>{title}</h3>
      <p style={{ marginBottom: 25, marginLeft: 25 }}>{description}</p>
      <p
        onClick={redirect}
        style={{
          color: "#FFCA05",
          fontSize: 20,
          cursor: "pointer",
          marginLeft: 25,
          paddingBottom: 20,
        }}
      >
        Read More
      </p>
    </div>
  );
};

export default TeamCard;
