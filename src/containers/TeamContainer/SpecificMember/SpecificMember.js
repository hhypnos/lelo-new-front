import React from "react";
import HeaderContainer from "../../HeaderContainer/HeaderContainer";
import TeamCard from "../TeamCard/TeamCard";
import ButtonContainer from "../../ButtonContainer/ButtonContainer";
import FooterContainer from "../../FooterContainer/FooterContainer";
import {useNavigate, useParams} from "react-router-dom";
import { Api } from "../../../services/Services";
import "./SpecificMember.css";

const SpecificMember = (props) => {
  const [teamMember, setTeamMember] = React.useState({});
  const [loading, setLoading] = React.useState(true);
  const [teams, setTeams] = React.useState([]);
  const { id } = useParams();
  // const data = props.teams.data.find((data) => data.id == memberId);
  console.log(props, "asda");
  React.useEffect(() => {
    Api.getData(`teams/${id}`).then((json) => {
      const res = json.result;

      setTeamMember(res);
    });
    Api.getData(`teams?order_by=asc&type_id=1&lang_id=1&data_per_page=4`).then(
      (json) => {
        const res = json.result.data;
        console.log(res, "res");
        setTeams(res);
        setLoading(false);
      }
    );
  }, [id]);
  // for navigate with :id u just need to use variable data
  const teamCardList = teams.map((event) => {
    if (event.id < 5) {
      return (
        <div key={event.id} className="teamLists-wrapper">
          <div className="first-teamCard">
            <TeamCard
              key={event.id}
              image={event.avatar.upload.full_path}
              title={event.full_name}
              description={event.position}
              id={event.id}
            />
          </div>
        </div>
      );
    }
  });

  const navigate = useNavigate();

  const redirect = () => {
    navigate("/more-teams");
  };
  return (
    <>
      {loading ? (
        <div>asdad</div>
      ) : (
        <div>
          <div className="specific-wrapper">
            <div className="header-div">
              {" "}
              <HeaderContainer />{" "}
            </div>
            <div className="specific-content-wrapper">
              <div>
                <div>
                  <img
                    className="memberImg"
                    src={teamMember.data.avatar.upload.full_path}
                    alt="member"
                  />
                </div>
                <div className="member-informations">
                  <h2 className="information--title">
                    {teamMember.data.full_name}
                  </h2>
                  <h5 className="information--description">
                    {teamMember.data.position}
                  </h5>
                  <p className="inofrmation--text">{teamMember.data.text}</p>
                </div>
              </div>
            </div>
            <div className="card-list--wrapper">{teamCardList}</div>
            <div onClick={redirect}>
            <ButtonContainer name="მეტის ნახვა" buttonStyle={true} />
            </div>
            <div className="organizations">
              <div className="organization-wrapper">
                <p className="choose-organization">ქალთა ორგანიზაცია</p>
              </div>
              <div className="organization-wrapper">
                <p className="choose-organization">ახალგაზრდული ორგანიზაცია</p>
              </div>
            </div>
            <div className="footer-div">
              <FooterContainer />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default SpecificMember;
