import React from "react";
import Circle from "../../images/yellowCircle.svg";
import TeamCard from "./TeamCard/TeamCard";
import "./TeamContainer.css";
import ButtonContainer from "../ButtonContainer/ButtonContainer";
import { useNavigate } from "react-router-dom";

const TeamContainer = (props) => {
  console.log(props, "asdasd");
  const navigate = useNavigate();

  const redirect = () => {
    navigate("/more-teams");
  };

  const teamCardList = props?.teams?.map((event) => {
    if (event.id < 5) {
      return (
        <TeamCard
          key={event.id}
          image={event.avatar.upload.full_path}
          title={event.full_name}
          id={event.id}
          description={event.position}
        />
      );
    }
  });
  return (
    <>
      {props.loading ? (
        <di>sodsd</di>
      ) : (
        <div>
          <div>
            <div style={{ display: "flex", marginBottom: 75 }}>
              <h2 style={{ fontSize: 54 }}>ჩვენი გუნდი</h2>
            </div>
            <div style={{ display: "flex", gap: 25, paddingBottom: 60 }}>
              {teamCardList}
            </div>
            <div onClick={redirect}>
              <ButtonContainer name="მეტის ნახვა" buttonStyle={true} />
            </div>
            <div className="organizations">
              <div className="organization-wrapper">
                <p className="choose-organization">ქალთა ორგანიზაცია</p>
              </div>
              <div className="organization-wrapper">
                <p className="choose-organization">ახალგაზრდული ორგანიზაცია</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default TeamContainer;
