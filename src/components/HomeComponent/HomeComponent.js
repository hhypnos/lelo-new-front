import React from "react";
import HeaderContainer from "../../containers/HeaderContainer/HeaderContainer";
import ContentContainer from "../../containers/ContentContainer/ContentContainer";
import NewsContainer from "../../containers/NewsContainer/NewsContainer";
import PlanContainer from "../../containers/PlanContainer/PlanContainer";
import TeamContainer from "../../containers/TeamContainer/TeamContainer";
import ContactContainer from "../../containers/ContactContainer/ContactContainer";
import FooterContainer from "../../containers/FooterContainer/FooterContainer";
import { Api } from "../../services/Services";
import Session from "../../images/session.svg";
import Bank from "../../images/bank.svg";
import ElectionIcon from "../../images/election.svg";
import "./HomeComponent.css";

const HomeComponent = ({ newsData, teamsData, teams, loading,loadingNews }) => {
  return (
    <>
      {loading ? (
        <div>iah</div>
      ) : (
        <div className="App">
          <HeaderContainer />
          <ContentContainer
            title="გაეცანით მარშალის გეგმას"
            image={Session}
            circle="0"
          />
          <div className="news-wrapper">
            <NewsContainer newsData={newsData} loadingNews={loadingNews} />
          </div>
          <PlanContainer />
          <ContentContainer title="ჩვენს შესახებ" image={Bank} circle="1" />
          <TeamContainer
            teamsData={teamsData}
            teams={teams}
            loading={loading}
          />
          <div className="electionContent">
            <ContentContainer
              circle="2"
              title="შიდა არჩევნები"
              image={ElectionIcon}
            />
          </div>
          <ContactContainer />
          <FooterContainer />
        </div>
      )}
    </>
  );
};

export default HomeComponent;
