const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "800px",
  tabletL: "1000px",
  Mac: "1495px",
  laptop: "1500px",
  laptopL: "1700px",
  desktop: "2560px",
};

export const device = {
  mobileToTablet: `screen and (min-device-width: ${size.mobileS}) and (max-device-width:${size.tablet})`,
  tabletToLaptop: `screen and (min-device-width: ${size.tablet}) and (max-device-width:${size.laptopL})`,
  tabletLToMac: `screen and (min-device-width: ${size.tabletL}) and (max-device-width:${size.laptop})`,
  laptopToLaptop: `screen and (min-device-width: ${size.laptop}) and (max-device-width:${size.laptopL})`,
  LaptopToDesktop: `screen and (min-device-width: ${size.mobileS}) and (max-device-width:${size.tablet})`,
};
