export const Api = {
  baseUrl: "http://138.68.125.120/api/",
  getData: function (url, params, method = "get", isFormData = false) {
    let headers = {};
    if (!isFormData) {
      headers = {
        // mode: "corse",
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      };
    } else {
      headers = {
        // mode: "corse",
        Accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      };
    }
    return fetch(this.baseUrl + url, {
      method: method.toUpperCase(),
      headers,
      body: JSON.stringify(params),
    }).then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error("");
      }
    });
  },
};
