import "./App.css";
import React from "react";
import HomeComponent from "./components/HomeComponent/HomeComponent";
import ReadMoreContainer from "./containers/ReadMoreContainer/ReadMoreContainer";
import ShowMore from "./containers/ShowMore/ShowMore";
import { Routes, Route } from "react-router-dom";
import ShowMoreTeams from "./containers/ShowMoreTeams/ShowMoreTeams";
import AboutUsContainer from "./containers/AboutUsContainer/AboutUsContainer";
import SpecificMember from "./containers/TeamContainer/SpecificMember/SpecificMember";
import Authorization from "./containers/Authorization/Authorization";
import ElectionsFilter from "./containers/ElectionsFilter/ElectionsFilter";
import SpecificElection from "./containers/ElectionsFilter/SpecificElection/SpecificElection";
import { Api } from "./services/Services";

function App({ newsData, teamsData, readMoreData }) {
  const [teams, setTeams] = React.useState([]);
  const [news, setNews] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [loadingNews, setLoadingNews] = React.useState(false);

  React.useEffect(() => {
      load();
  }, []);

const load = ()=>{
    Api.getData(`teams?order_by=asc&type_id=1&lang_id=1&data_per_page=4`).then(
        (json) => {
            const res = json.result.data;
            setTeams(res);
        }
    );
    Api.getData(`blogs?order_by=asc&lang_id=1&type_id=1&data_per_page=4`).then(
        (json) => {
            const res = json.result.data;
            setNews(res);
            console.log(res,'news')
        }
    );
}

  return (
    <div className="App">
      <Routes>
        <Route
          exact
          path="/"
          element={
            <HomeComponent
              newsData={news}
              teamsData={teamsData}
              loading={loading}
              loadingNews={loadingNews}
              teams={teams}
            />
          }
        />
        <Route
          path="/more"
          element={
            <ShowMore newsData={news} teams={teams} loading={loading} />
          }
        />
        <Route
          path="/read-more/:id"
          element={
            <ReadMoreContainer
              readMoreData={readMoreData}
              newsData={news}
              teams={teams}
              loading={loading}
            />
          }
        />
        <Route
          exact
          path="/more-teams"
          element={
            <ShowMoreTeams
              newsData={newsData}
              teamsData={teamsData}
              teams={teams}
              loading={loading}
            />
          }
        />
        <Route
          exact
          path="/aboutUs"
          element={
            <AboutUsContainer
              newsData={newsData}
              readMoreData={readMoreData}
              teams={teams}
              loading={loading}
            />
          }
        />
        <Route
          path="/specific-member/:id"
          element={
            <SpecificMember
              teamsData={teamsData}
              teams={teams}
              loading={loading}
            />
          }
        />
        <Route
          path="/log-in"
          element={<Authorization teams={teams} loading={loading} />}
        />
        <Route
          path="/elections"
          element={<ElectionsFilter teams={teams} loading={loading} />}
        />
        <Route
          path="/specific-election"
          element={
            <SpecificElection
              teamsData={teamsData}
              teams={teams}
              loading={loading}
            />
          }
        />
      </Routes>
    </div>
  );
}

export default App;
